package jp.kshoji;

import android.util.Log;

/**
 * Created by guest1 on 4/5/2016.
 */
public class Timber {
    public static void d(String tag, String msg) {
        AbstractCentralActivity.diaLog += msg+"\n";
        Log.d(tag, msg);
    }
    public static void clear() {
        AbstractCentralActivity.diaLog = "";
    }
}
