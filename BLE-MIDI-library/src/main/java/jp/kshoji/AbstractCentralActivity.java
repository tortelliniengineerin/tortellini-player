package jp.kshoji;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import jp.kshoji.blemidi.listener.OnMidiInputEventListener;

/**
 * Created by guest1 on 4/5/2016.
 */
public abstract class AbstractCentralActivity extends AppCompatActivity {
    public static Activity centralActivity;
    public static String diaLog = "";
    public static OnMidiInputEventListener onMidiInputEventListener;
    public abstract OnMidiInputEventListener getMidiEventListener();
}
