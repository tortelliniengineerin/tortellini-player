package jp.kshoji.blemidi.sample;

import android.app.Activity;
import android.support.wearable.activity.WearableActivity;

import jp.kshoji.blemidi.listener.OnMidiInputEventListener;

/**
 * This is the same as the AbstractCentralActivity but with Wearable features included
 * Created by Nick on 4/25/2016.
 */
public abstract class WearableAbstractCentralActivity extends WearableActivity {
    public static Activity centralActivity;
    public static String diaLog = "";
    public static OnMidiInputEventListener onMidiInputEventListener;
    public abstract OnMidiInputEventListener getMidiEventListener();
}
